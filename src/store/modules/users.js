import { MUTATION_TYPES } from '../mutationTypes';
import { instance } from '@/services/service'

export default {
  state: {
    usersList: []
  },

  actions: {
    getUsers({commit}) {
      instance.get('/users')
        .then(response => {
          commit(MUTATION_TYPES.GET_USERS, response.data)
          console.log('response: ', response.data);
        })
        .catch(e => {
          throw new Error(`API ${e}`);
        })
    },

    sortByChooseOption({commit}, option) {
      commit(MUTATION_TYPES.SORT_ITEM, option)
    }
  },
 
  mutations: {
    [MUTATION_TYPES.GET_USERS](state, user) {
      state.usersList = user
    },

    [MUTATION_TYPES.SORT_ITEM](state, option) {
      if(option === 'city') {
        state.usersList.sort((a, b) => {
          return (a.address.city > b.address.city) ? 1 : -1;
        })
      }

      if(option === 'name') {
        state.usersList.sort((a, b) => {
          return (a.name > b.name) ? 1: -1;
        })
      }

      if(option === 'username') {
        state.usersList.sort((a, b) => {
          return (a.username > b.username) ? 1: -1;
        })
      }
    }
  },

  getters: {
    getAllUsers(state) {
      return state.usersList;
    }
  },

  namespaced: true
}